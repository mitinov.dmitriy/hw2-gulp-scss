window.addEventListener("load", function () {
    const menuBtn = document.querySelector(".navigation__menu-button");
    const navMenu = document.querySelector(".header__menu");

    showHideMenu(768, navMenu);

    window.addEventListener("resize", function () {
        showHideMenu(768, navMenu);
    })

    menuBtn.addEventListener("click", function (evt) {
        const backgrounds = evt.target.closest("button").children;
        for (const background of backgrounds) {
            if (background.classList.contains("btn__decor--unclicked")){
                background.classList.remove("btn__decor--unclicked");
            }
            else {
                background.classList.add("btn__decor--unclicked");
            }
        }
       if (navMenu.classList.contains("hidden")){
           navMenu.classList.remove("hidden");
       }
       else {
           navMenu.classList.add("hidden");
       }
    })

    function showHideMenu(width, elemToOperate) {
        if (window.innerWidth > width) {
            elemToOperate.classList.remove("hidden");
        }
        else {
            elemToOperate.classList.add("hidden");
        }
    }
})